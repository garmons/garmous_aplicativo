import 'package:flutter/material.dart';

class IconCategoria extends StatelessWidget {
  final String assets;
  final String label;

  const IconCategoria({
    super.key,
    required this.assets,
    required this.label,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 60,
      width: 78,
      padding: const EdgeInsets.all(10),
      decoration: BoxDecoration(
          color: const Color(0x0ff0f0f0),
          border: Border.all(color: const Color(0xffc2c2c2)),
          borderRadius: BorderRadius.circular(16)),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(bottom: 4 ),
            child: Image.asset(
              assets,
              width: 41,
            ),
          ),
          Text(
            label,
            style: const TextStyle(
              fontSize: 11,
              fontWeight: FontWeight.bold,
              color: Color(0xff707070),
            ),
          )
        ],
      ),
    );
  }
}
