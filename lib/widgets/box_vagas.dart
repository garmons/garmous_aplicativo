import 'package:flutter/material.dart';

class BoxVaga extends StatelessWidget {
  final String anunciovaga;

  const BoxVaga({
    super.key,
    required this.anunciovaga,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(7),
      child: Image.asset(
        anunciovaga,
        width: 200,
      ),
    );
  }
}
