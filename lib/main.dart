import 'package:flutter/material.dart';
import 'package:garmous_aplicativo/pages/login_page.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(),
      home: const LoginPage(),
    ),
  );
}
