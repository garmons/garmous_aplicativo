import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color(0xff23282B),
        child: Center(
          child: Column(
            children: [
              const SizedBox(
                height: 50,
              ),
              Image.asset(
                'assets/logogarmous.png',
                width: 200,
              ),
              const SizedBox(
                height: 80,
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Padding(
                      padding: EdgeInsets.all(40.0),
                      child: TextField(
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          labelText: 'E-mail',
                          hintText: 'Ex: email@gmail.com',
                        ),
                      )

                      // Text(
                      //   'Email',
                      //   style: TextStyle(
                      //     color: Color(0xffFFFFFF),
                      //     fontSize: 20,
                      //   ),
                      // ),
                      ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
