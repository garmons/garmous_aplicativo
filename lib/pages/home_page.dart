import 'package:flutter/material.dart';
import '../widgets/box_icon_categorias.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            const Padding(
              padding: EdgeInsets.all(12.0),
              child: TextField(
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                  prefixIcon: Icon(Icons.search),
                  hintText: 'Digite sua busca...',
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(50.0),
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 10, bottom: 10),
              child: SizedBox(
                height: 80,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: const [
                    SizedBox(
                      width: 10,
                    ),
                    IconCategoria(
                      assets: 'assets/ic_garcom.png',
                      label: 'Garçom',
                    ),
                    SizedBox(
                      width: 7,
                    ),
                    IconCategoria(
                      assets: 'assets/ic_barman.png',
                      label: 'Barman',
                    ),
                    SizedBox(
                      width: 7,
                    ),
                    IconCategoria(
                      assets: 'assets/ic_cozinheiro.png',
                      label: 'Cozinheiro',
                    ),
                    SizedBox(
                      width: 7,
                    ),
                    IconCategoria(
                      assets: 'assets/ic_atendente.png',
                      label: 'Atendente',
                    ),
                    SizedBox(
                      width: 7,
                    ),
                    IconCategoria(
                      assets: 'assets/ic_entregador.png',
                      label: 'Entregador',
                    ),
                    SizedBox(
                      width: 10,
                    ),
                  ],
                ),
              ),
            ),
            const Padding(
              padding: EdgeInsets.only(bottom: 10, top: 10),
              child: Text(
                "Profissionais",
                style: TextStyle(
                  fontSize: 15,
                ),
              ),
            ),
            SizedBox(
              child: ListView.builder(
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: 10,
                itemBuilder: (context, index) {
                  return const Card();
                },

                // children: const [
                //   BoxVaga(anunciovaga: 'assets/vaga1.jpg'),
                //   BoxVaga(anunciovaga: 'assets/vaga1.jpg'),
                //   BoxVaga(anunciovaga: 'assets/vaga1.jpg'),
                //   BoxVaga(anunciovaga: 'assets/vaga1.jpg'),
                // ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
